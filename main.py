import asyncio
import datetime
import os
from time import sleep

import pynetbox
import urllib3
from promisio import promisify, Promise

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_tenants_sync(nb):
    print([t.slug for t in nb.tenancy.tenants.all()])  # this is an io sync call
    sleep(1)  # definitively not async


@promisify
async def get_tenants_prom(nb):  # same as previous, but with the @promisify and async
    print([t.slug for t in nb.tenancy.tenants.all()])  # this is an io sync call
    sleep(1)  # definitively not async


async def main():
    nb = pynetbox.api(url=os.environ["URL"], token=os.environ["TOKEN"], threading=True)
    nb.http_session.verify = False
    x = datetime.datetime.now()
    y = x
    print(f"start {y - x}")
    get_tenants_sync(nb)
    x = y
    y = datetime.datetime.now()
    print(f"after first sync call {y - x}")
    get_tenants_sync(nb)
    x = y
    y = datetime.datetime.now()
    print(f"after second sync call {y - x}")
    p1 = get_tenants_prom(nb)
    x = y
    y = datetime.datetime.now()
    print(f"after first async call {y - x}")
    p2 = get_tenants_prom(nb)
    x = y
    y = datetime.datetime.now()
    print(f"after second async call {y - x}")
    await Promise.all([p1, p2])
    x = y
    y = datetime.datetime.now()
    print(f"after awaiting promises {y - x}")


if __name__ == '__main__':
    asyncio.run(main())
