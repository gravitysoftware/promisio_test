# PROMISIO-TEST

Mixing sync request using pynetbox and make them async thanks to promisio

## Setup

(using poetry)

```
poetry install
```

### Run
```
PYTHONUNBUFFERED=1 URL=https://netbox.example.com/ TOKEN=netboxtoken poetry run python main.py
```

### Output

```
start 0:00:00
['a', 'b', 'c']
after first sync call 0:00:01.258946
['a', 'b', 'c']
after second sync call 0:00:01.135890
after first async call 0:00:00.000176
after second async call 0:00:00.000087
['a', 'b', 'c']
['a', 'b', 'c']
after awaiting promises 0:00:02.354878
```
